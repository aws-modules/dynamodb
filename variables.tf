variable "application" {
  type    = string
  default = null
}
variable "environment" {
  type = string
}
variable "product" {
  type = string
}
variable "hash_key" {
  type = map(any)
}
variable "use_case" {
  type = string
}


variable "billing_mode" {
  type    = string
  default = "PAY_PER_REQUEST"
}
variable "point_in_time_recovery" {
  type    = bool
  default = true
}
variable "replica_region" {
  type    = map(any)
  default = null
}
variable "server_side_encryption" {
  type    = bool
  default = true
}
variable "stream_enabled" {
  type    = bool
  default = true
}
variable "stream_view_type" {
  type    = string
  default = "NEW_AND_OLD_IMAGES"
}
variable "range_key" {
  type    = map(any)
  default = {}
}

variable "global_secondary_index" {
  type    = map(any)
  default = {}
}
variable "global_secondary_index_attributes" {
  type    = map(any)
  default = {}
}
