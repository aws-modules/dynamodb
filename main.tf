resource "aws_dynamodb_table" "this" {
  name         = var.application != null ? "${var.environment}_${var.product}_${var.application}_${var.use_case}" : "${var.environment}_${var.product}_${var.use_case}"
  billing_mode = var.billing_mode
  hash_key     = var.hash_key["name"]
  range_key    = length(keys(var.range_key)) == 0 ? null : var.range_key["name"]

  attribute {
    name = var.hash_key["name"]
    type = var.hash_key["type"]
  }

  dynamic "attribute" {
    for_each = length(keys(var.range_key)) == 0 ? {} : tomap({ "range_key" = var.range_key })
    content {
      name = attribute.value.name
      type = attribute.value.type
    }
  }

  dynamic "attribute" {
    for_each = var.global_secondary_index_attributes
    content {
      name = attribute.key
      type = attribute.value.type
    }
  }

  dynamic "replica" {
    for_each = var.replica_region == null ? {} : var.replica_region
    content {
      region_name = replica.key
    }
  }

  stream_enabled   = var.stream_enabled
  stream_view_type = var.stream_view_type

  tags = var.application != null ? {
    Environment             = var.environment
    Product                 = var.product
    Application             = var.application
    Use_case                = var.use_case
    Can_be_deleted          = true
    Created_using_terraform = true
    } : {
    Environment             = var.environment
    Product                 = var.product
    Use_case                = var.use_case
    Can_be_deleted          = true
    Created_using_terraform = true
  }

  point_in_time_recovery {
    enabled = var.point_in_time_recovery
  }

  server_side_encryption {
    enabled = var.server_side_encryption
  }

  dynamic "global_secondary_index" {
    for_each = var.global_secondary_index
    content {
      name               = global_secondary_index.key
      hash_key           = global_secondary_index.value.hash_key
      range_key          = global_secondary_index.value.range_key
      projection_type    = global_secondary_index.value.projection_type
      non_key_attributes = global_secondary_index.value.non_key_attributes
    }
  }
}
